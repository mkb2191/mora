import sys
if sys.version_info.major == 2:
    import tkMessageBox as messagebox
    import tkFileDialog as filedialog
    import Tkinter as tk
    import ttk
else:
    from tkinter import messagebox
    from tkinter import filedialog
    from tkinter import ttk
    import tkinter as tk

class NewURL(ttk.Frame):
    def __init__(self, callback, master=None):
        ttk.Frame.__init__(self, master)
        ttk.Label(self, text='URL:').grid(row=0, column=0)
        self.url = tk.Text(self, width=80, height=1)
        self.url.grid(row=0, column=1)
        ttk.Label(self, text='Crawl Depth:').grid(row=0, column=2)
        self.depth = tk.Variable(self)
        ttk.OptionMenu(self, self.depth, '0', *map(str, range(10)),
                       'No Max').grid(row=0, column=3)
        ttk.Button(self, text='Add', command=self.add).grid(row=0, column=4)
        self.callback = callback

    def add(self):
        url = self.url.get('1.0', 'end-1c')
        depth = self.depth.get()
        self.callback(url, depth)

class Status(ttk.Frame):
    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)
        ttk.Label(self, text='Current URL:').grid(row=0, column=0)
        self.current_url = ttk.Label(self, text='N/A')
        self.current_url.grid(row=0, column=1)

class GUI(ttk.Frame):
    def __init__(self, master=None):
        ttk.Frame.__init__(self, master)
        self.newurl = NewURL(self.add, self)
        self.newurl.grid(row=0, column=0, sticky='w')
        self.status = Status(self)
        self.status.grid(row=1, column=0, sticky='w')

    def add(self, url, depth):
        print(url, depth)

if __name__ == '__main__':
    gui = GUI()
    gui.grid()
    gui.mainloop()
